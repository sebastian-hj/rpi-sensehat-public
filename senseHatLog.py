import sys
import time
import argparse
import json
import csv
import socket
import threading
from datetime import datetime
from sense_hat import SenseHat
from pydrive.drive import GoogleDrive
from pydrive.auth import GoogleAuth
import Adafruit_IO

# Reference
# https://www.raspberrypi.org/learning/sense-hat-data-logger/worksheet/

# Global variables
past_hour = 0
gdrive_queue = list()

########################
# Time-related functions
########################
def get_timestamp():
    return datetime.now().strftime('%Y%m%d%A_%H:%M:%S')

def get_year_month_day():
    return datetime.now().strftime('%Y%m%d')


def get_hour():
    return int(datetime.now().strftime('%H'))


def check_hour(sense_data):
    global past_hour
    # If a new hour, print current sense data (except at night...)
    current_hour = get_hour()
    if current_hour != past_hour and 7 <= current_hour <= 23:
        # Update past hour
        past_hour = current_hour
        # Show sense data on SenseHat
        show_sense_data(sense_data)
        # Print sense data on console
        print_sense_data(sense_data)


############
# Formatting
############
def get_headings():
    header = ['Timestamp', 'Humidity', 'Temperature Humidity',
              'Temperature Pressure', 'Pressure']
    return header


############################
# Internet-related functions
############################
def is_connected():
    # Reference: https://stackoverflow.com/a/20913928
    try:
        host = socket.gethostbyname('www.google.com')
        s = socket.create_connection((host, 80), 2)
        return True
    except:
        pass
    return False


##########################
# gDrive-related functions
##########################
def initialize_gdrive():
    gauth = None
    drive = None

    # Requires 'settings.yaml' and 'client_secrets.json'

    # TODO: validate 'settings.yaml' is present in directory
    gauth = GoogleAuth(settings_file='settings.yaml')
    
    # Refresh credentials (requires generated 'credentials.json' file at the same location)
    # Reference: http://stackoverflow.com/questions/24419188/automating-pydrive-verification-process
    if gauth.credentials is None:
        gauth.CommandLineAuth()
    elif gauth.access_token_expired:
        gauth.Refresh()
    else:
        gauth.Authorize()
    
    # Configure
    drive = GoogleDrive(gauth)

    return gauth, drive


def upload_to_gdrive(sense_hat_file, gauth, drive, folder_id):
    # Refresh token if expired
    if gauth.access_token_expired:
        gauth.Refresh()
    try:
        f = drive.CreateFile({'parents': [{'kind': 'drive#fileLink',
                                            'id': folder_id}]})
        f.SetContentFile(sense_hat_file)
        f.Upload()
        print 'Uploaded {0} to gDrive!'.format(sense_hat_file)
    except Exception as global_inst:
        print 'Error uploading to gDrive'
        print type(global_inst)     # the exception instance
        print global_inst.args      # arguments stored in .args
        print global_inst           # __str__ allows args to printed directly


def check_thread_gdrive(sleep_time, folder_id):
    while True:
        time.sleep(sleep_time)
        # Check if there is internet connection
        if is_connected() is True:
            # If there is, upload files in queue until queue is empty
            while gdrive_queue:
                # Upload to gDrive
                upload_to_gdrive(gdrive_queue[0], gauth, drive, folder_id)
                # Remove file from queue
                gdrive_queue.pop(0)


############################
# SenseHat-related functions
############################
def get_sense_data():
    # Current date and time
    time_stamp = get_timestamp()

    # Get humidity
    humidity = sense.get_humidity()
    # Get temperature from humidity
    temp_humidity = sense.get_temperature_from_humidity()
    # Get temperature from pressure
    temp_pressure = sense.get_temperature_from_pressure()
    # Get pressure
    pressure = sense.get_pressure()

    # Sensor Data
    sense_data = [humidity, temp_humidity, temp_pressure, pressure]
    sense_data = ['{0:.4f}'.format(measure) for measure in sense_data]
    # Add timestamp
    sense_data.insert(0, time_stamp)

    return sense_data


def show_sense_data(sense_data):
    headings = ['Time', 'H', 'TH', 'TP', 'P']

    for x in xrange(0, len(sense_data)):
        # Get current heading and data
        current_heading = headings[x]
        current_data = sense_data[x]
        # For time data, display only HH:MM:ss
        if x == 0:
            current_data = current_data[-8:]
            print current_data
        # Show message on SenseHat
        sense.show_message('{0}: {1}'.format(current_heading, current_data),
                           scroll_speed = 0.08,
                           text_colour = [50, 50, 50])


def print_sense_data(sense_data):
    headings = get_headings()

    for x in xrange(0, len(sense_data)):
        print '{0}: {1}'.format(headings[x], sense_data[x])


###########
# Files I/O
###########
def open_json_to_dict(file_path):
    try:
        conf_dict = json.load(open(file_path))
    except IOError:
        sys.exit('Cannot find configuration file {0}!'.format(file_path))
    except ValueError:
        sys.exit('Configuration file {0} not in JSON format!'.format(file_path))
    
    return conf_dict


def write_2D_list_to_csv(file_path, a_list):
    try:
        with open(file_path, 'wb') as file_id:
            writer = csv.writer(file_id)
            writer.writerows(a_list)
    except IOError:
        sys.exit('ERROR: cannot write CSV file ''{0}'''.format(file_path))


#############
# Adafruit IO
#############
def send_to_adafruitIO_feed(feed_name, feed_value):
    try:
        aio.send(feed_name, float(feed_value))
    except Adafruit_IO.AdafruitIOError as inst:
        print 'Error uploading to Adafruit IO'
        print type(inst)     # the exception instance
        print inst.args      # arguments stored in .args
        print inst           # __str__ allows args to printed directly
        # x, y = inst          # __getitem__ allows args to be unpacked directly
        # print 'x =', x
        # print 'y =', y
    except Exception as global_inst:
        print 'Error uploading to Adafruit IO'
        print type(global_inst)     # the exception instance
        print global_inst.args      # arguments stored in .args
        print global_inst           # __str__ allows args to printed directly


def upload_to_adafruitIO(current_sense_data, feeds):
    # Check if there is internet connection
    if is_connected() is True:

        for x in xrange(0, feeds['number']):
            send_to_adafruitIO_feed(feeds['names'][x], current_sense_data[x+1])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Sense Hat data logger.')
    parser.add_argument('config', nargs='?', type=str, help='Input JSON configuration file.')
    args = parser.parse_args()

    # Set configuration file path
    # If no argument is provided, assume same directory and 'configuration.json' name
    if args.config is None:
        conf_file = 'configuration.json'
    else:
        conf_file = args.config
    
    # Read configuration file
    conf_dict = open_json_to_dict(conf_file)

    is_gdrive_enable = conf_dict['google_drive']['enable']
    is_adafruitio_enable = conf_dict['adafruit_io']['enable']


    # Use gDrive only if requested
    if is_gdrive_enable == 1:
        # Start background thread
        # Reference:
        # https://gist.github.com/sebdah/832219525541e059aefa
        thread = threading.Thread(target=check_thread_gdrive,
                                  args=(conf_dict['google_drive']['sleep_time'],
                                        conf_dict['google_drive']['folder_id']))
        # Daemonize thread
        thread.daemon = True
        # Start execution
        thread.start()

        # Initialize gDrive
        gauth, drive = initialize_gdrive()

    # Start SenseHat
    sense = SenseHat()
    # Clear LEDs
    sense.clear()
    # Define time to sleep (s)
    sleep_time = 30

    # Use Adafruit IO only if requested
    if is_adafruitio_enable == 1:
        # Initialize Adafruit IO
        adafruit_key = conf_dict['adafruit_io']['key']
        aio = Adafruit_IO.Client(adafruit_key)
    
    # Daily list of data
    daily_list = list()
    # Append headings
    daily_list.append(get_headings())

    # Initialize past day and hour
    past_day = get_year_month_day()
    past_hour = get_hour()

    while True:
        try:
            # Sleep...
            time.sleep(sleep_time)

            # Check if days are the same
            current_day = get_year_month_day()
            if current_day != past_day:
                # Update past day
                past_day = current_day
                
                # Set file name
                sense_hat_file = '{0}.csv'.format(get_year_month_day())
                # Write to CSV
                write_2D_list_to_csv(sense_hat_file, daily_list)

                # Add file to gDrive queue
                if is_gdrive_enable == 1:
                    gdrive_queue.append(sense_hat_file)

                # Empty list
                daily_list = list()
                # Appeand headings to new list
                daily_list.append(get_headings())

            # Get current data
            current_sense_data = get_sense_data()

            # Upload to Adafruit IO
            if is_adafruitio_enable == 1:
                upload_to_adafruitIO(current_sense_data, conf_dict['adafruit_io']['feeds'])

            # Check hour to see if measurements should be displayed
            check_hour(current_sense_data)

            # Append to list
            daily_list.append(current_sense_data)

        except KeyboardInterrupt:
            # Write to CSV
            write_2D_list_to_csv('{0}_interrupted.csv'.format(get_year_month_day()),
                                 daily_list)
            
            # Say goodbye...
            print ''
            print 'Sorry to see you go...'

            # Exit
            sys.exit(0)
