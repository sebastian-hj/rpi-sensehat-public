## Introduction

### Description

- Use Raspberry Pi and Sense Hat to log temperature, humidity, and pressure.
- Use Sense Hat LEDs to display hourly measurements. 
- Upload daily measurements to Google Drive in CSV format.
- Add measurements to Adafruit IO.

## Installation

### Requirements

- Raspberry Pi: tested on Raspberry Pi 2 and 3.
- Sense Hat Python library: [sense-hat](http://pythonhosted.org/sense-hat/)
- Google Drive Python library: [PyDrive](https://pythonhosted.org/PyDrive/)
- Adafruit IO Python library: [adafruit-io](https://github.com/adafruit/io-client-python)

### Configuration File

A _configuration.json_ file is used to enable different features and specify their settings.

#### Sense Hat

    "sense_hat": {
        "sleep_time": 30
    }

- `sensehat.sleep_time`: interval (in seconds) for which data from Sense Hat is read. Default to thirty seconds.

#### Google Drive

    "google_drive": {
        "enable": 1,
        "folder_id": "",
        "sleep_time": 3600
    }

- `google_drive.enable`: enable/disable Google Drive integration.
- `google_drive.folder_id`: Google Drive folder ID to which files will be uploaded to.
- `google_drive.sleep_time`: interval (in seconds) for which files are checked to be uploaded to Google Drive. Default to one hour.

#### Adafruit IO

    "adafruit_io": {
        "enable": 1,
        "key": "",
        "feeds": {
            "number": 4,
            "names": ["", "", "", ""]
        }
    }

- `adafruit_io.enable`: enable/disable Adafruit IO integration.
- `adafruit_io.key`: Adafruit IO key for personal account.
- `adafruit_io.feeds`: Adafruit IO feeds to upload data to.
    - `adafruit_io.feeds.number`: number of feeds.
    - `adafruit_io.feeds.names`: name of feeds.


### Process

To be explained...
